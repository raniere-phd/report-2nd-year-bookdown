# 2nd Year Report

Preview: https://raniere-phd.gitlab.io/report-2nd-year/report.html

## Guidebook for Research Degree Studies 2020/21

1. After the qualifying period, students must submit progress reports (typed and in English)
   on an annual basis until they have submitted the final version of their thesis for oral
   examination and completed any other academic requirements.

2. Students should submit three copies of the annual progress report (with Form SGS36) to
   their research supervisors for forwarding to the Qualifying Panel for assessment. On the
   basis of the annual progress report, the Qualifying Panel should make a recommendation
   regarding the student’s suitability to continue his or her studies for approval by the
   School/Department.