# Pre-sale radiographic examination views

Pre-sale radiographic examination views,
see Table\ \@ref(tab:pre-sale-radiographic-views),
cover six bone groups:
carpus,
front fetlock,
hind fetlock,
tarsus,
stifle,
and
front hoof.
<!--
All bone groups are from fore and hindlimbs,
see Figure\ \@ref(fig:all-examination-view-location).
-->

<!--
The 48 standardised views [@veterinaryregulationwelfareandbiosecuritypolicyHongKongJockey2020]
specified by The Hong Kong Jockey Club
are listed in the Table\ \@ref(tab:pre-sale-radiographic-views).
-->

```{r include=FALSE, message=FALSE}
df <- read_csv("data/pre-sale-radiographic-examination-views.csv")
```

(ref:pre-sale-radiographic-views) Pre-sale radiographic examination views (for left and right side).

```{r pre-sale-radiographic-views, echo=FALSE, message=FALSE}
df[
  1:24,
  c("Bone Group", "View")
] %>%
  knitr::kable(
    booktabs = TRUE,
    caption = '(ref:pre-sale-radiographic-views)',
    longtable=FALSE
)
```

(ref:all-examination-view-location) Illustration of carpus, fetlock, tarsus, stifle and hoof location. Image adapted from Horse anatomy.svg by Wikipedian Prolific and vectorization process by Wilfredor. Original image available at https://commons.wikimedia.org/wiki/File:Horse_anatomy.svg and licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.

```{r all-examination-view-location, eval=FALSE, fig.align='center', fig.cap="(ref:all-examination-view-location)", fig.show='hold', include=FALSE, out.width = global_out_width}
knitr::include_graphics(
    './img/all-examination-view-location.svg'
)
```

## Carpus

Carpus
is the cluster of bones
between the radius and the metacarpus,
see Figure\ \@ref(fig:horse-anatomy-carpus).
Common conditions in the carpus include
synovitis, <!-- inflammation in the connective tissue that lines the inside of the joint capsule -->
capsulitis, <!-- inflammation in the ligaments -->
cartilage wear and erosion,
and
chip fractures.

(ref:horse-anatomy-carpus) Illustration of carpus location. Image adapted from Horse anatomy.svg by Wikipedian Prolific and vectorization process by Wilfredor. Original image available at https://commons.wikimedia.org/wiki/File:Horse_anatomy.svg and licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.

```{r horse-anatomy-carpus, echo = FALSE, fig.cap = "(ref:horse-anatomy-carpus)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/carpus/horse_anatomy.png'
)
```

### Dorsopalmar (DPa) projection

<!-- https://blog.imv-imaging.co.uk/blog/2019/april/radiography-techniques-of-the-equine-fetlock-joint -->

The carpus dorsopalmar projection
is, more accurately,
the dorso10°proximal-palmarodistal oblique
and
the x-ray beam is center at the level of the condyles
of the third metacarpal bone and collimate.
The accessory carpal bone should be in the left side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-carpus-dp).

(ref:horse-anatomy-carpus-dp) (A) Illustration of left carpus dorsopalmar radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia from left carpus dorsopalmar radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left carpus dorsopalmar radiograph.

```{r horse-anatomy-carpus-dp, echo = FALSE, fig.cap = "(ref:horse-anatomy-carpus-dp)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/carpus/l-fore-carpus-dp.png'
)
```

### Dorsolateral to palmaromedial oblique (D55L‐PaMO) projection

<!-- https://blog.imv-imaging.co.uk/blog/2019/april/radiography-techniques-of-the-equine-fetlock-joint -->

The dorsolateral to palmaromedial oblique projection
has the x-ray beam horizontal and at a 55° angle to the dorsopalmar axis of the leg
center at the level of the condyles of the third metacarpal bone and collimate.
The accessory carpal bone should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-carpus-dlpmo).

(ref:horse-anatomy-carpus-dlpmo) (A) Illustration of left carpus dorsolateral to palmaromedial oblique radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left carpus dorsolateral to palmaromedial oblique radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left carpus dorsolateral to palmaromedial oblique radiograph.

```{r horse-anatomy-carpus-dlpmo, echo = FALSE, fig.cap = "(ref:horse-anatomy-carpus-dlpmo)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/carpus/l-fore-carpus-dlpmo.png'
)
```

### Dorsomedial to palmarolateral oblique (D75M‐PaLO) projection

The dorsomedial to palmarolateral oblique projection
has the x-ray beam horizontal and at a 75° angle to the dorsomedial axis of the leg
center at the level of the condyles of the third metacarpal bone and collimate.
The accessory carpal bone should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-carpus-dmplo).

(ref:horse-anatomy-carpus-dmplo) (A) Illustration of left carpus dorsomedial to palmarolateral oblique radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left carpus dorsomedial to palmarolateral oblique radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left carpus dorsomedial to palmarolateral oblique radiograph.

```{r horse-anatomy-carpus-dmplo, echo = FALSE, fig.cap = "(ref:horse-anatomy-carpus-dmplo)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/carpus/l-fore-carpus-dmplo.png'
)
```

The first carpal bone is visible
In the dorsomedial to palmarolateral oblique projection.
This bone is absent in the majority of horses
and,
when it is present,
it is consider normal.
This bone is confused with an osteochondral fragment ("fracture").

### Flexed LM projection

The flexed lateromedial projection
has the x-ray beam perpendicular to the dorsopalmar direction of the carpus.
The accessory carpal bone should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-carpus-flexed-lm).

(ref:horse-anatomy-carpus-flexed-lm) (A) Illustration of left carpus flexed lm radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left carpus dorsomedial to palmarolateral oblique radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left carpus dorsomedial to palmarolateral oblique radiograph.

```{r horse-anatomy-carpus-flexed-lm, echo = FALSE, fig.cap = "(ref:horse-anatomy-carpus-flexed-lm)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/carpus/l-fore-carpus-flexed-lm.png'
)
```

### Flexed D60ºPR – DDIS-TAL Oblique projection

The accessory proximal carpal bone
and
the 4th carpal bone
should be in the left side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-carpus-flexed-dp).

(ref:horse-anatomy-carpus-flexed-dp) (A) Illustration of left carpus dorsomedial to palmarolateral oblique radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left carpus dorsomedial to palmarolateral oblique radiograph. (C) Masks for semantic segmentation of left carpus dorsomedial to palmarolateral oblique radiograph. (D) Radiopaque anatomical markers and masks for semantic segmentation of distal carpal row on left carpus dorsomedial to palmarolateral oblique radiograph.

```{r horse-anatomy-carpus-flexed-dp, echo = FALSE, fig.cap = "(ref:horse-anatomy-carpus-flexed-dp)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/carpus/l-fore-carpus-flexed-dp.png'
)
```

## Front Fetlock

Front fetlock is the common name for the metacarpophalangeal joints
and
include
third metacarpus,
phalanx
and
proximal sesamoids,
see Figure\ \@ref(fig:horse-anatomy-fetlock-front).
The fetlock is the single most common region affected
by lameness\ [@carpenterOccupationalRelatedLamenessConditions2020].
Common areas of cartilage
and
osteochondral trauma
involve the dorsal rim of P1,
the proximal sesamoid,
and
distal metarcarpus or metatarsus\ [@carpenterOccupationalRelatedLamenessConditions2020].

(ref:horse-anatomy-fetlock-front) Illustration of front fetlock location. Image adapted from Horse anatomy.svg by Wikipedian Prolific and vectorization process by Wilfredor. Original image available at https://commons.wikimedia.org/wiki/File:Horse_anatomy.svg and licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.

```{r horse-anatomy-fetlock-front, echo = FALSE, fig.cap = "(ref:horse-anatomy-fetlock-front)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/front/horse_anatomy.png'
)
```

### DPa projection

For the front fetlock dorsopalmar projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The lateral condyle is smaller than the medial condyle
and
should be in the left side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-fore-fetlock-dp).

(ref:horse-anatomy-fore-fetlock-dp) (A) Illustration of left front fetlock DPa radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left front fetlock DPa radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left front fetlock DPa radiograph.

```{r horse-anatomy-fore-fetlock-dp, echo = FALSE, fig.cap = "(ref:horse-anatomy-fore-fetlock-dp)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/front/l-fore-fetlock-dp.png'
)
```

### D45ºL – PAMO projection

For the front fetlock dorsolateral to palmaromedial oblique projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The proximal sesamoids should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-fore-fetlock-dlpmo).

(ref:horse-anatomy-fore-fetlock-dlpmo) (A) Illustration of left fore fetlock D45L-PAMO radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left fore fetlock D45L-PAMO radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left fore fetlock D45L-PAMO radiograph.

```{r horse-anatomy-fore-fetlock-dlpmo, echo = FALSE, fig.cap = "(ref:horse-anatomy-fore-fetlock-dlpmo)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/front/l-fore-fetlock-dlpmo.png'
)
```

### D45ºM – PALO projection

For the front fetlock dorsomedial to palmarolateral oblique projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The proximal sesamoids should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-fore-fetlock-dmplo).

(ref:horse-anatomy-fore-fetlock-dmplo) (A) Illustration of left front fetlock D45M-PALO radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left front fetlock D45M-PALO radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left front fetlock D45M-PALO radiograph.

```{r horse-anatomy-fore-fetlock-dmplo, echo = FALSE, fig.cap = "(ref:horse-anatomy-fore-fetlock-dmplo)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/front/l-fore-fetlock-dmplo.png'
)
```

### Flexed LM projection

For the front fetlock flexed lateromedial projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The proximal sesamoids should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-fore-fetlock-flexed-lm).

(ref:horse-anatomy-fore-fetlock-flexed-lm) (A) Illustration of left front fetlock flexed LM radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left front fetlock flexed LM radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left front fetlock flexed LM radiograph.

```{r horse-anatomy-fore-fetlock-flexed-lm, echo = FALSE, fig.cap = "(ref:horse-anatomy-fore-fetlock-flexed-lm)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/front/l-fore-fetlock-flexed-lm.png'
)
```

### Flexed D125º Distal – PAPR Oblique projection

For the front fetlock flexed dorsopalmar projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The lateral condyle is smaller than the medial condyle
and
should be in the left side of the radiograph,,
see Figure\ \@ref(fig:horse-anatomy-fore-fetlock-flexed-dp).

(ref:horse-anatomy-fore-fetlock-flexed-dp) (A) Illustration of left front fetlock flexed DD-PAPR radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left front fetlock flexed DD-PAPR radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left front fetlock flexed DD-PAPR radiograph.

```{r horse-anatomy-fore-fetlock-flexed-dp, echo = FALSE, fig.cap = "(ref:horse-anatomy-fore-fetlock-flexed-dp)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/front/l-fore-fetlock-flexed-dp.png'
)
```

### LM projection

For the front fetlock lateromedial projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The proximal sesamoids should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-fore-fetlock-lm).

(ref:horse-anatomy-fore-fetlock-lm) (A) Illustration of left front fetlock LM radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left front fetlock LM radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left front fetlock LM radiograph.

```{r horse-anatomy-fore-fetlock-lm, echo = FALSE, fig.cap = "(ref:horse-anatomy-fore-fetlock-lm)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/front/l-fore-fetlock-lm.png'
)
```

## Hind Fetlock

Hind fetlock is the common name for the metatarsophalangeal joints
and
include
third metatarsus,
phalanx
and
proximal sesamoids,
see Figure\ \@ref(fig:horse-anatomy-fetlock-hind).

(ref:horse-anatomy-fetlock-hind) Illustration of hind fetlock location. Image adapted from Horse anatomy.svg by Wikipedian Prolific and vectorization process by Wilfredor. Original image available at https://commons.wikimedia.org/wiki/File:Horse_anatomy.svg and licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.

```{r horse-anatomy-fetlock-hind, echo = FALSE, fig.cap = "(ref:horse-anatomy-fetlock-hind)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/hind/horse_anatomy.png'
)
```

### DPL projection

For the hind fetlock dorsopalmar projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The lateral condyle is smaller than the medial condyle
and
should be in the left side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-hind-fetlock-dp).

(ref:horse-anatomy-hind-fetlock-dp) (A) Illustration of left hind fetlock DPL radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left front fetlock DPL radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left front fetlock DPL radiograph.

```{r horse-anatomy-hind-fetlock-dp, echo = FALSE, fig.cap = "(ref:horse-anatomy-hind-fetlock-dp)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/hind/l-hind-fetlock-dp.png'
)
```

### D45ºL – PLMO projection

For the hind fetlock dorsolateral to palmaromedial oblique projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The proximal sesamoids should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-hind-fetlock-dlpmo).

(ref:horse-anatomy-hind-fetlock-dlpmo) (A) Illustration of left hind fetlock D45L-PLMO radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left hind fetlock D45L-PLMO radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left hind fetlock D45L-PLMO radiograph.

```{r horse-anatomy-hind-fetlock-dlpmo, echo = FALSE, fig.cap = "(ref:horse-anatomy-hind-fetlock-dlpmo)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/hind/l-hind-fetlock-dlpmo.png'
)
```

### D45ºM – PLLO projection

For the hind fetlock dorsomedial to palmarolateral oblique projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The proximal sesamoids should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-hind-fetlock-dmplo).

(ref:horse-anatomy-hind-fetlock-dmplo) (A) Illustration of left hind fetlock D45M-PLLO radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left hind fetlock D45M-PLLO radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left hind fetlock D45M-PLLO radiograph.

```{r horse-anatomy-hind-fetlock-dmplo, echo = FALSE, fig.cap = "(ref:horse-anatomy-hind-fetlock-dmplo)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/hind/l-hind-fetlock-dmplo.png'
)
```

### LM projection

For the hind fetlock lateromedial projection,
the x-ray beam is center at the level of the proximal sesamoids
and
collimate.
The proximal sesamoids should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-hind-fetlock-lm).

(ref:horse-anatomy-hind-fetlock-lm) (A) Illustration of left hind fetlock LM radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left hind fetlock LM radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left hind fetlock LM radiograph.

```{r horse-anatomy-hind-fetlock-lm, echo = FALSE, fig.cap = "(ref:horse-anatomy-hind-fetlock-lm)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/fetlock/hind/l-hind-fetlock-lm.png'
)
```

## Tarsus

Tarsus,
also called hock,
is the common name for the bones and joints between the tibia and the metatarsus
and
include
talus,
calcaneus,
and
tarsus,
see Figure\ \@ref(fig:horse-anatomy-tarsus).

(ref:horse-anatomy-tarsus) Illustration of tarsus location. Image adapted from Horse anatomy.svg by Wikipedian Prolific and vectorization process by Wilfredor. Original image available at https://commons.wikimedia.org/wiki/File:Horse_anatomy.svg and licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.

```{r horse-anatomy-tarsus, echo = FALSE, fig.cap = "(ref:horse-anatomy-tarsus)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/tarsus/horse_anatomy.png'
)
```

### DPL projection

For the tarsus dorsopalmar projection,
the x-ray beam is center at the level of the calcaneus bone
and
collimate.
The calcaneus bone should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-tarsus-dp).

(ref:horse-anatomy-tarsus-dp) (A) Illustration of left tarsu DPL radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left tarsu DPL radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left tarsu DPL radiograph.

```{r horse-anatomy-tarsus-dp, echo = FALSE, fig.cap = "(ref:horse-anatomy-tarsus-dp)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/tarsus/l-hind-tarsus-dp.png'
)
```

### D65ºM – PLLO projection

For the tarsus dorsolateral to palmaromedial oblique projection,
the x-ray beam is center at the level of the calcaneus bone
and
collimate.
The calcaneus bone should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-tarsus-dmplo).

(ref:horse-anatomy-tarsus-dmplo) (A) Illustration of left tarsus D65M-PLLO radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left tarsus D65M-PLLO radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left tarsus D65M-PLLO radiograph.

```{r horse-anatomy-tarsus-dmplo, echo = FALSE, fig.cap = "(ref:horse-anatomy-tarsus-dmplo)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/tarsus/l-hind-tarsus-dmplo.png'
)
```

### D10ºL – PLMO projection

For the tarsus dorsomedial to palmarolateral oblique projection,
the x-ray beam is center at the level of the calcaneus bone
and
collimate.
The calcaneus bone should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-tarsus-dlpmo).

(ref:horse-anatomy-tarsus-dlpmo) (A) Illustration of left tarsus D10L-PLMO radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left tarsus D10L-PLMO radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left tarsus D10L-PLMO radiograph.

```{r horse-anatomy-tarsus-dlpmo, echo = FALSE, fig.cap = "(ref:horse-anatomy-tarsus-dlpmo)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/tarsus/l-hind-tarsus-dlpmo.png'
)
```

### LM projection

For the tarsus lateromedial projection,
the x-ray beam is center at the level of the calcaneus bone
and
collimate.
The calcaneus bone should be in the right side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-tarsus-lm).

(ref:horse-anatomy-tarsus-lm) (A) Illustration of left tarsus LM radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left tarsus LM radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left tarsus LM radiograph.

```{r horse-anatomy-tarsus-lm, echo = FALSE, fig.cap = "(ref:horse-anatomy-tarsus-lm)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/tarsus/l-hind-tarsus-lm.png'
)
```

## Stifle

<!-- https://aaep.org/sites/default/files/issues/RadiographWilson.pdf -->

Stifle,
is the common name for the bones and joints between the femur and the tibia
and
include
the patella,
see Figure\ \@ref(fig:horse-anatomy-stifle).

(ref:horse-anatomy-stifle) Illustration of stifle location. Image adapted from Horse anatomy.svg by Wikipedian Prolific and vectorization process by Wilfredor. Original image available at https://commons.wikimedia.org/wiki/File:Horse_anatomy.svg and licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.

```{r horse-anatomy-stifle, echo = FALSE, fig.cap = "(ref:horse-anatomy-stifle)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/stifle/horse_anatomy.png'
)
```

### LM projection

For the stifle lateromedial projection,
the x-ray beam is center at the level of the join of femur and tibia
and
collimate.
The patella bone should be in the left side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-stifle-lm).

(ref:horse-anatomy-stifle-lm) (A) Illustration of left stifle LM radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in red from left stifle LM radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left stifle LM radiograph.

```{r horse-anatomy-stifle-lm, echo = FALSE, fig.cap = "(ref:horse-anatomy-stifle-lm)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/stifle/l-hind-stifle-lm.png'
)
```

### CD – CR projection

For the stifle CD – CR projection,
the x-ray beam is center at the level of the join of femur and tibia
and
collimate.
The fibula should be in the left side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-stifle-cd-cr).

(ref:horse-anatomy-stifle-cd-cr) (A) Illustration of left stifle CD-CR radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in red from left stifle CD-CR radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left stifle CD-CR radiograph.

```{r horse-anatomy-stifle-cd-cr, echo = FALSE, fig.cap = "(ref:horse-anatomy-stifle-cd-cr)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/stifle/l-hind-stifle-cd-cr.png'
)
```

### CDL – CRMO projection

For the stifle CDL – CRMO projection,
the x-ray beam is center at the level of the join of femur and tibia
and
collimate.
The patella bone and fibula should be in the left side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-stifle-cdl-crmo).

(ref:horse-anatomy-stifle-cdl-crmo) (A) Illustration of left stifle CDL-CRMO radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in red from left stifle CDL-CRMO radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left stifle CDL-CRMO radiograph.

```{r horse-anatomy-stifle-cdl-crmo, echo = FALSE, fig.cap = "(ref:horse-anatomy-stifle-cdl-crmo)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/stifle/l-hind-stifle-cdl-crmo.png'
)
```

## Front hoof

Hoof is the tip of a toe (distal phalanx) cover by a thick layer of keratin,
see Figure\ \@ref(fig:horse-anatomy-stifle).
The most common fractures are
the lateral wing of the left forelimb
and
medial wing of the right forelimb\ [@carpenterOccupationalRelatedLamenessConditions2020].
Fractures may not be apparent on radiographs
until 7--10 days after they occur\ [@carpenterOccupationalRelatedLamenessConditions2020].

(ref:horse-anatomy-hoof) Illustration of front hoof location. Image adapted from Horse anatomy.svg by Wikipedian Prolific and vectorization process by Wilfredor. Original image available at https://commons.wikimedia.org/wiki/File:Horse_anatomy.svg and licensed under the Creative Commons Attribution-Share Alike 3.0 Unported license.

```{r horse-anatomy-hoof, echo = FALSE, fig.cap = "(ref:horse-anatomy-hoof)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/hoof/horse_anatomy.png'
)
```

### Lateralmedial

For the hoof lateromedial projection,
the x-ray beam is center at the level of the middle phalanx
and
collimate.
The distal phalanx should be in the left side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-stifle-lm).

(ref:horse-anatomy-foot-lm) (A) Illustration of left fore foot lateralmedial radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in red from left fore foot lateralmedial radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left fore foot lateralmedial radiograph.

```{r horse-anatomy-foot-lm, echo = FALSE, fig.cap = "(ref:horse-anatomy-foot-lm)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/hoof/l-fore-foot-lm.png'
)
```

### DorsoPalmar

For the hoof dorsopalmar projection,
the x-ray beam is center at the level of the distal phalanx
and
collimate.
The lateral should be in the left side of the radiograph,
see Figure\ \@ref(fig:horse-anatomy-stifle-lm).

(ref:horse-anatomy-foot-dp) (A) Illustration of left fore foot dorsopalmar radiograph provided by The Hong Kong Jockey Club and reproduced with permission. (B) Region of interest in fuchsia and common region of abnormality in red from left fore foot dorsopalmar radiograph. (C) Radiopaque anatomical markers and masks for semantic segmentation of left fore foot dorsopalmar radiograph.

```{r horse-anatomy-foot-dp, echo = FALSE, fig.cap = "(ref:horse-anatomy-foot-dp)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/hoof/l-fore-foot-dp.png'
)
```
