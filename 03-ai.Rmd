# Artificial Intelligence

<!--
Deep learning (DL) is a subset of machine learning (ML),
which is itself a subset of artificial intelligence (AI).
I named the section using artificial intelligence
to be as general as possible
-->

Artificial intelligence
is the logic capacity that enables machines to predict the answer to a problem,
for example
reply to a given text
or
provide the name of a object in a photo.
Until the 1990s,
artificial intelligence used many if-then-else statements
to replicate expert logic\ [@litjensSurveyDeepLearning2017].
After the 1990s,
artificial intelligence replaced the many if-then-else statements
with statistical model\ [@litjensSurveyDeepLearning2017],
which is now called machine learning,
a subset of artificial intelligence.
Machine learning,
see Figure\ \@ref(fig:artificial-intelligence),
can be subset in two groups:
supervised
and
unsupervised.
Part of the supervised group
is called deep learning,
a class of complex neural networks.

(ref:artificial-intelligence) Illustration of relationship between artificial intelligence, machine learning and deep learning.

```{r artificial-intelligence, echo = FALSE, fig.cap = "(ref:artificial-intelligence)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/artificial-intelligence.png'
)
```

Machine learning models
find features (also called patterns) in the data
during the training
and
use the found features to make predictions,
see Figure\ \@ref(fig:ai-training-and-prediction).
<!--
Machine learning models
had two main parts:
feature extraction
and
statistical classifier of extracted features\ [@litjensSurveyDeepLearning2017].
-->
In the classical machine learning models,
the selection and extraction of grayscale, texture and shape features
was done by experts
--
a legacy from the early days of artificial intelligence.
Today,
machine learning models have millions of weights (also called parameters)
that allow the model itself to select and extract the features\ [@litjensSurveyDeepLearning2017].

(ref:ai-training-and-prediction) Illustration of machine learning pipeline. Research data is split into training data and testing data. Training data is used to calibrate the millions of weights in the model. Once the weights are calibrated, prediction is obtained using the testing data and prediction quality metrics are calculated. If suitable prediction quality metrics were obtained, the model is used to make predictions from new data.

```{r ai-training-and-prediction, echo = FALSE, fig.cap = "(ref:ai-training-and-prediction)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/ai-training-and-prediction.png'
)
```

The accuracy of a machine learning model is dependent of the research data
used during training\ [@jainEffectRadiologyReport2021].
Ideally,
machine learning models would be trained with a data
and
validated with a data that is independent of the one used for training
to reduce bias\ [@kocakHowReadReview2021; @wynantsPredictionModelsDiagnosis2020; @robertsCommonPitfallsRecommendations2021].
In practice,
the research data is randomly split into training and testing data
and
cross-validation\ [@stoneCrossValidatoryChoiceAssessment1974] is used
as safe guard against selection bias during the training and testing data split.

In the next sections,
the reader will find a brief explanation of
classical unsupervised machine learning,
classical supervised machine learning,
and
logistic regression.
Neural networks and deep learning is covered in Chapter\ \@ref(neural-networks)

## Classical Unsupervised Machine Learning

Classical unsupervised machine learning methods do not incorporate the outcomes
during training.
Examples of classical unsupervised machine learning methods are
k-means clustering,
hierarchical clustering,
and
Gaussian mixture clustering\ [@jiangArtificialIntelligenceHealthcare2017].

The k-means clustering prediction\ [@lloydLeastSquaresQuantization1982],
see Figure\ \@ref(fig:k-means-clustering-prediction),
takes a point, $x$,
and
a set of means, $K$.
The point $x$ is predicted to belong to the closest cluster.
For the prediction,
the distance $d_i$ between
the point $x$
and
every mean $k_i$
($k_i \in K$)
is calculated
and the prediction is based on the smallest $d_i$.

(ref:k-means-clustering-prediction) Illustration of k-means clustering prediction. Squares represent points, circles represent means, and colours represent clusters. (a) Initial configuration of the problem. (b) Calculation of distance $d_1$ from the point to red mean. (c) Calculation of distance $d_2$ from the point to blue mean. (d) Final configuration with the point being classified as red because $d_1 < d_2$.

```{r k-means-clustering-prediction, echo = FALSE, fig.cap = "(ref:k-means-clustering-prediction)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/k-means-clustering-prediction.png'
)
```

The k-means clustering training\ [@lloydLeastSquaresQuantization1982],
see Figure\ \@ref(fig:k-means-clustering-training),
is an iterative method that
takes an initial set of points, $X$,
and
an initial set of means, $K^0$.
At each iteration $i \geq 1$,
every point is assigned to one of the closest means (or clusters)
and
the set of means, $K^i$, is calculated.
The training stops when the set of means is preserved, $K^{i - 1} = K^i$,
or
after a maximum number of iterations.

(ref:k-means-clustering-training) Illustration of k-means clustering training. Squares represent points, circles represent means, and colours represent clusters. (a) Initial configuration of the problem. (b) Configuration at the end of 1st iteration. (c) Configuration at the end of 2nd iteration. (d) Final configuration.

```{r k-means-clustering-training, echo = FALSE, fig.cap = "(ref:k-means-clustering-training)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/k-means-clustering-training.png'
)
```

## Classical Supervised Machine Learning

Classical supervised machine learning methods incorporate the outcomes
during training.
Examples of classical unsupervised machine learning methods are
linear regression,
logistic regression,
naïve Bayes,
decision tree,
nearest neighbour,
random forest,
discriminant analysis,
support vector machine,
and
neural network\ [@jiangArtificialIntelligenceHealthcare2017].

Support vector machine prediction\ [@cortesSupportvectorNetworks1995],
see Figure\ \@ref(fig:support-vector-machine-prediction),
takes a point, $x$,
and
a hyperplane.
The point $x$ is predicted to belong to the region it is located
considering the hyperplane.
For the prediction,
the normal vector to the hyperplane passing through $x$ is calculated
and
the prediction is based on the sign of the normal vector.

(ref:support-vector-machine-prediction) Illustration of support vector machine prediction. Squares represent points, green line represents hyperplane, and colours regions represent classes. (a) Initial configuration of the problem. (b) Calculation of normal vector to the hyperplane passing through point. (c) Final configuration with the point being classified as red.

```{r support-vector-machine-prediction, echo = FALSE, fig.cap = "(ref:support-vector-machine-prediction)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/support-vector-machine-prediction.png'
)
```

The support vector machine training\ [@cortesSupportvectorNetworks1995],
see Figure\ \@ref(fig:support-vector-machine-training),
is an iterative method that
takes an initial set of points, $X$,
the classification of every points, $Y$,
and
an initial hyperplane.
At each iteration,
a descent direction
and
step size
are calculated for the hyperplane
and
the hyperplane is updated using
the descent direction
and
step size.

(ref:support-vector-machine-training) Illustration of support vector machine training. Squares represent points, green line represents hyperplane, and colours represent classes. (a) Initial configuration of the problem. (b) Configuration at the end of first iteration. (c) Configuration at the end of second iteration. (d) Final configuration.

```{r support-vector-machine-training, echo = FALSE, fig.cap = "(ref:support-vector-machine-training)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/support-vector-machine-training.png'
)
```

## Logistic Regression

Logistic regression is a classical supervised machine learning method.
It is introduced here
because it is a classification algorithm
adopted by the machine learning community
and
it is a "one-neuron" neural network.
Neural networks with two or more neurons will be explored in Section\ \@ref(neural-networks).

In biology,
neurons,
see Figure\ \@ref(fig:neuron-biology),
are a type of cells that
have,
in addition to the cell body,
many dendrites
and
a long axon.
Neurons receive afferent signal thought the dendrites
and,
if the cumulative effect of the signal reaches the critical threshold,
send efferent signal through the axon.

(ref:neuron-biology) Diagram of biologic neuron with cell body in blue, dendrites in red and axon in green.

```{r neuron-biology, echo = FALSE, fig.cap = "(ref:neuron-biology)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/neuron-biology.png'
)
```

In the late 1950s,
a mathematical model to represent the behaviour of the neuron
was proposed
[@rosenblattPerceptronProbabilisticModel1958].
The artificial neuron,
see Figure\ \@ref(fig:neuron-computer-science),
has weights associated with each dendrite
and
a critical threshold.
The artificial neuron
receives input thought each dendrite,
aggregates the inputs,
and
produces an output
based on the aggregate value and the critical threshold.

(ref:neuron-computer-science) Diagram of artificial neuron. $x_1$, $x_2$, and $x_3$ are inputs. $w_1$, $w_2$, and $w_3$ are weights. $t$ is the critical threshold. $y'$ is the output produced.

```{r neuron-computer-science, echo = FALSE, fig.cap = "(ref:neuron-computer-science)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/neuron-computer-science.png'
)
```

The logistic regression prediction
takes
inputs $x$,
weights $w$,
an activation function $\sigma$,
and a critical threshold $t$.
For the prediction $y'$,
the following calculations will be performed:
\begin{align*} 
z &= w x \\
y' &= \begin{cases}
0, \text{if } \sigma(z) \leq t; \\
1, \text{otherwise.}
\end{cases}
\end{align*}
Popular choices for the activation function $\sigma$,
see Figure\ \@ref(fig:activation-function-plot),
are
the sigmoid function
(also called logistic function),
the hyperbolic tangent function
(also called tanh),
and
the rectified linear unit function
(also called ReLU)
that are defined, respectively, by
\begin{align*}
&\frac{1}{1 + e^{-z}},\\
&\frac{e^{z} - e^{-z}}{e^{z} + e^{-z}},
\end{align*}
and
$$\text{max}(0, z).$$

```{r activation-function, include = FALSE}
sigmoid_function <- function(z) {
  return(1/(1 + exp(-z)))
}

tanh_function <- function(z) {
  return((exp(z) - exp(-z))/(exp(z) + exp(-z)))
}

relu_function <- function(z) {
  return(max(c(0, z)))
}

x = seq(
  from = -2,
  to = 2,
  by = 0.01
)
df <- rbind(
  data.frame(x = x, y = sapply(x, sigmoid_function), f = 'sigmoid'),
  data.frame(x = x, y = sapply(x, tanh_function), f = 'tanh'),
  data.frame(x = x, y = sapply(x, relu_function), f = 'ReLU')
)
```

(ref:activation-function-plot) Visualisation of sigmoid function, hyperbolic tangent function, and rectified linear unit function.

```{r activation-function-plot, echo = FALSE, fig.cap = "(ref:activation-function-plot)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
facet_order <- c('sigmoid', 'tanh', 'ReLU')

df %>%
  mutate(
    f = factor(f, levels=facet_order)
  ) %>%
  ggplot(
    aes(
      x,
      y
    )
  ) +
  geom_line() +
  xlab(if (knitr::is_latex_output()) "z" else "z") +
  ylab(if (knitr::is_latex_output()) "sigma(z)" else "σ(z)") +
  facet_wrap(vars(f))
```


```{r include=FALSE}
x <- c(1, 0.25)
w <- c(0.5, 0.5)
t <- 0.5

z <- w %*% x
sigma_z <- sigmoid_function(z)
y <- if(sigma_z < t) 0 else 1
```

As a small scale example,
consider
\begin{align*} 
x &= (1, 0.25), \\
w &= (0.5, 0.5), \\
\sigma(z) &= \frac{1}{1 + e^{-z}}, \\
t &= 0.5.
\end{align*}
Then
$z = `r z`$,
$\sigma(z) = `r sigma_z`$,
and
$y' = `r y`$.

Logistic regression prediction can be extended
to three or more non-mutually exclusive classes (also called labels)
by using a logistic regression prediction to each individual class.
For three or more mutually exclusive classes,
the individual predictions are aggregated
so that
$$y' = \arg \max \left( \frac{e^{z_i}}{\sum_{j} e^{z_j}} \right).$$
The formula $e^{z_i} / ( \sum_{j} e^{z_j} )$ is called
the softmax function
or
normalized exponential function.

The logistic regression training,
see Figure\ \@ref(fig:logistic-regression-training),
is an iterative method that takes
a set of points, $X$,
the classification of every point, $Y$,
a initial set of weights, $w^0$,
an activation function $\sigma$,
a initial critical threshold $t^0$,
and
a loss function (also called cost function) that gives the distance of the prediction $Y^i$ to $Y$.
At each iteration $i \geq 1$,
the following steps are executed:

1. calculate the prediction of every point, $Y^i$,
   based on $X$, $w^{i-1}$, $\sigma$, and $t^{i-1}$;
2. calculate a descent direction and step size
   based on the loss function for $Y^i$.
3. calculate $w^i$ and $t^i$
   based on the descent direction and step size.

(ref:logistic-regression-training) Illustration of logistic regression training. (a) Initial configuration of the problem. (b) Calculation of the prediction, $y^0$, for $x$. (c) Update of $w$ and $t$ based on the descent direction and step size previously calculated. (d) Final configuration.

```{r logistic-regression-training, echo = FALSE, fig.cap = "(ref:logistic-regression-training)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/logistic-regression-training.png'
)
```

When the dimension of every point in $X$ is very large
or
the number of points in $X$ is very large,
which usually is the case,
it will be impossible to calculate the prediction of every point simultaneously
and
the set of points $X$ will be split in subsets (also called batches).
At each iteration,
one subset will be used.
One pass through all set of points $X$ is called one epoch.
   
How to find
a initial set of weights, $w^0$,
a initial critical threshold, $t^0$,
and
a loss function that gives the distance of the prediction $Y^i$ to $Y$ is out of the scope of this report.
For more information,
see [@narkhedeReviewWeightInitialization2021] that covers weight initialization strategies
and
[@wangComprehensiveSurveyLoss2020] that covers loss functions.
Also,
calculate the descent direction and step size is out of the scope of this report.
For more information on descent direction,
see [@sunSurveyOptimizationMethods2020] that provides a mathematical background
and
[@dogoComparativeAnalysisGradient2018] that provides a computational comparison between methods.

After a certain number of iterations,
a phenomenal called overfitting,
see Figure\ \@ref(fig:overfitting),
may happen.
Overfitting is characterised by the model
achieving a perfect or near perfect prediction with the training data
and a poor prediction with any other data,
usually verified with the testing data.
Strategies to avoid overfitting exists
and
reader is referenced to [@kimComparisonMethodsReduce2020].

(ref:overfitting) Illustration of overfitting. Squares represent points, green line represents hyperplane, and colours represent classes. Training data in the top row and testing data in the botton row. (a) Initial configuration of the problem: three and four wrong prediction in training and testing data, respectively. (b) Configuration at the end of first iteration: four and two wrong prediction in training and testing data, respectively. (c) Configuration at the end of second iteration: one and one wrong prediction in training and testing data, respectively. (d) Final configuration with occurrence of overfitting: zero and four wrong prediction in training and testing data, respectively.

```{r overfitting, echo = FALSE, fig.cap = "(ref:overfitting)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
    './img/overfitting.png'
)
```