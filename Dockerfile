FROM ubuntu:rolling

# Enviorment variables
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Asia/Hong_Kong
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# Install dependencies
RUN apt-get update \
    && apt-get install -y \
    fonts-stix \
    inkscape \
    make \
    pandoc \
    pandoc-citeproc \
    r-base \
    r-cran-bookdown \
    r-cran-rmarkdown \
    r-cran-tidyverse \
    r-cran-xfun \
    && rm -rf /var/lib/apt/lists/*

CMD /bin/bash
