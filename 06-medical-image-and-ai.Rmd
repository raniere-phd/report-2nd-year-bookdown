# Medical Image and Artificial Intelligence

Medical images are created with a wide range of technologies,
for example
radiography,
magnetic resonance imaging,
positron emission tomography,
ultrasound,
endoscopy,
and
microscope photography.
Each technology is commonly used for some body systems
and
produce different images,
see Figure\ \@ref(fig:medical-image-types).
The analysis
of human and veterinary medical images
is a critical part of clinical diagnosis and decision-making [@liuAdvancesDeepLearning2021; @rehmanSurveyMedicalImage2021].
Artificial intelligence can be applied
to tasks,
such as
classification,
detection,
and
segmentation,
<!--and
registration,-->
that are imperative for the analyse of medical image
[@liuAdvancesDeepLearning2021]
and
share some similarity across different medical image technologies.
Artificial intelligence power tools that can be categorized as
helper,
replacer,
and
booster
depending of the tasks it performs
and
the output that it provides to the user\ [@bramvanginnekenAIRadiologistsPainful2018].

(ref:medical-image-types) Illustration of medical images produced with different technologies. (a) Right foot radiograph by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication. (b) Chest radiograph by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication. (c) Mammogram by SCiardullo used under Creative Commons Attribution-Share Alike 4.0 International. (d) Brain MR by Seannovak used under Creative Commons Attribution-Share Alike 4.0 International. (e) Fundus photograph of the right eye by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.  (f) Histology of pars distalis of the anterior pituitary by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r medical-image-types, echo = FALSE, fig.cap = "(ref:medical-image-types)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/medical-image-types.png'
)
```

Until 2021,
the literature covering application of artificial intelligence
in veterinary medical imaging
is smaller
than human medical imaging
[@basranRadiomicsPlatformComputing2021].
For comparison,
the literature covering veterinary medical imaging
is smaller than the literature covering one single human disease
based on Google Scholar:
11500 results for `("artificial intelligence" OR "machine learning" OR "deep learning") AND "veterinary medicine"`
and
18300 results for `("artificial intelligence" OR "machine learning" OR "deep learning") AND "COVID-19"`
on 29 April 2021.
A few literature survey and review papers on
artificial intelligence applied to veterinary medical image analysis
exist\ [@pinarcihanReviewMachineLearning2017; @awayshehReviewMedicalDecision2019]
and
more on artificial intelligence applied to human medical image analysis,
see
Table\ \@ref(tab:ai-applied-task)
and
Table\ \@ref(tab:ai-applied-system).
<!-- Using (review OR survey) AND ("medical image" OR "medical applications") AND "machine learning" on Google Scholar -->


```{r include=FALSE, message=FALSE}
ai_applied <- read_csv("./data/artificial-intelligence-applied-to-medical-image-analysis-review.csv")
ai_applied <- ai_applied[ai_applied$Include == TRUE,]
ai_applied$Year <- ai_applied$Reference %>%
  str_sub(-5, -2) %>%
  as.integer()
ai_applied <- ai_applied %>%
  arrange(desc(Year), Reference)
ai_applied[ai_applied == "Yes"] <- '+'
ai_applied[ai_applied == "No"] <- '-'
```

(ref:ai-applied-task) Tasks in medical image analysis covered in literature survey and review papers of artificial intelligence applied to human medical images.

```{r ai-applied-task, echo=FALSE, message=FALSE}
ai_applied %>%
  select(
    Reference,
    Classification,
    Detection,
    Segmentation
    #Registration
  ) %>%
  knitr::kable(
    "pipe",
    booktabs = TRUE,
    caption = '(ref:ai-applied-task)'
)
```

(ref:ai-applied-system) Body systems covered in literature survey and review papers of artificial intelligence applied to human medical images.

```{r ai-applied-system, echo=FALSE, message=FALSE}
ai_applied %>%
  select(
    Reference,
    `Skeletal System`,
    `Respiratory System`,
    `Breast`,
    `Nervous System`,
    `Ophthalmic System`,
    `Histology, Pathology and Microscopy`
    #`Cardiovascular System`,
    #`Digestive System`,
  ) %>%
  knitr::kable(
    "pipe",
    booktabs = TRUE,
    caption = '(ref:ai-applied-system)'
)
```

<!--
Radiographs pose some challenges to be analyse by artificial intelligence,
for example:
image size
(medical images can be $100,000 \times 100,000$\ pixels
when artificial intelligence is limited to $200 \times 200$ pixels)\ [@andreDeepLearningenabledMedical2021].
-->

The following sections
provide more details about artificial intelligence
in the scope of each task previous mentioned.
Artificial intelligence models employed in each task are explored in Section\ \@ref(neural-networks).

## Regression

The regression task is
to assign probabilities to predefined labels by analysing the entire image,
see Figure\ \@ref(fig:analyse-medical-image-tasks-regression-prediction).
The output of regression task is used in other tasks,
see Section\ \@ref(classification),
but has limited use as end product.

(ref:analyse-medical-image-tasks-regression-prediction) Illustration of artificial intelligence prediction for regression of medical image. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-regression-prediction, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-regression-prediction)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/regression-prediction.png'
)
```

The dataset used
to train artificial intelligence
for regression
has
images
and
the labels of each image,
see Figure\ \@ref(fig:analyse-medical-image-tasks-regression-training).

(ref:analyse-medical-image-tasks-regression-training) Illustration of artificial intelligence training for regression of medical image.  X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-regression-training, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-regression-training)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/regression-training.png'
)
```

## Classification

The classification task is
to assign one or more predefined labels to an input image by analysing the entire image\ [@litjensSurveyDeepLearning2017; @calliDeepLearningChest2021; @liuAdvancesDeepLearning2021],
see Figure\ \@ref(fig:analyse-medical-image-tasks-classification-prediction).
The classification task can be defined as a regression task
followed by output reduction to the class with higher probability.
In the medical imaging domain,
classification is typically used when the expected output is a single variable,
for example fracture present\ [@olczakArtificialIntelligenceAnalyzing2017]
and
bone age\ [@leeFullyAutomatedDeep2017].

(ref:analyse-medical-image-tasks-classification-prediction) Illustration of artificial intelligence prediction for classification of medical image. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-classification-prediction, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-classification-prediction)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/classification-prediction.png'
)
```

In the binary classification task,
the prediction will be one of four possible outcomes:
the model correctly predicts the positive label,
called True Positive (TP);
the model correctly predicts the negative label,
called True Negative (TN);
the model incorrectly predicts the positive label,
called False Positive (FP);
and
the model incorrectly predicts the negative label,
called False Negative (FN).
The number of observation for each possible outcomes
can be organised in a table
called confusion matrix, 
see Figure\ \@ref(fig:analyse-medical-image-tasks-classification-confusion-matrix).

(ref:analyse-medical-image-tasks-classification-confusion-matrix) Illustration of confusion matrix and metrics to evaluate the quality of the model.

```{r analyse-medical-image-tasks-classification-confusion-matrix, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-classification-confusion-matrix)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/classification-confusion-matrix.png'
)
```

Confusion matrix is extremely transparent
as it provides to the reader the raw number for each (truth label, predicted label) pair
and
can be extended to multi-label classification tasks.
Read the confusion matrix is time consuming
and
a more direct metric to evaluate the quality of the model is desired,
for example
accuracy (A),
precision (P),
and
sensitivity (S)
that are defined, respectively, as
\begin{align*}
&\frac{\text{TP} + \text{TN}}{\text{TP} + \text{TN} + \text{FP} + \text{FN}}, \\
&\frac{\text{TP}}{\text{TP} + \text{FP}},
\end{align*}
and
$$\frac{\text{TP}}{\text{TP} + \text{FN}}.$$
One binary classification model that has higher accuracy, precision, or sensitivity than another model
is consider to provide predictions of better quality.
When classes differ in size,
accuracy, precision, and sensitivity
might be misleading
and other metrics should be use.
For multi-label classification task,
accuracy,
precision,
and
sensitivity
is not appropriate
and some average is used\ [@everinghamPascalVisualObject2010].

The four possible outcomes mentioned earlier are connected,
see Figure\ \@ref(fig:analyse-medical-image-tasks-classification-probability-density).
One way to find the optimal value for the threshold $t$
is to use the receiver operating characteristic curve (ROC curve),
which is the visualisation of true positive rate ($\text{TP} / (\text{TP} + \text{FN})$) against false positive rate ($\text{FP} / (\text{FP} + \text{TN})$) at various values of threshold.
As a small scale example,
consider the data represented in Table\ \@ref(tab:roc-curve).
Using Table\ \@ref(tab:roc-curve),
it is possible to calculate the true positive rate
and
the false positive rate
and
create the ROC curve,
see Figure\ \@ref(fig:roc-curve-plot).
The optimal value for the threshold $t$ is the one responsible for
the point in the ROC curve that is closes to the point given by
true positive rate equals 1 and false positive rate equals 0.

(ref:analyse-medical-image-tasks-classification-probability-density) Illustration of distribution of possible outcomes for the binary classification task.

```{r analyse-medical-image-tasks-classification-probability-density, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-classification-probability-density)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/classification-probability-density.png'
)
```

(ref:roc-curve) Data for small scale example of receiver operating characteristic curve.

```{r roc-curve, echo=FALSE, message=FALSE}
id <- seq(
  from = 1,
  to = 10,
  by = 1
)
y <- c(0, 0, 0, 0, 0, 1, 1, 1, 1, 1)
sigma <- c(0.10, 0.32, 0.48, 0.54, 0.61, 0.49, 0.51, 0.63, 0.77, 0.91)
y1 <- ifelse(sigma <= 0.2, 0, 1)
y2 <- ifelse(sigma <= 0.4, 0, 1)
y3 <- ifelse(sigma <= 0.5, 0, 1)
y4 <- ifelse(sigma <= 0.6, 0, 1)
y5 <- ifelse(sigma <= 0.9, 0, 1)

df <- data.frame(
  id = id,
  y = y,
  sigma = sigma,
  y1 = y1,
  y2 = y2,
  y3 = y3,
  y4 = y4,
  y5 = y5
)

df %>%
  knitr::kable(
    booktabs = TRUE,
    caption = '(ref:roc-curve)',
    col.names = c(
      'ID',
      'y',
      'σ(z)',
      'y (t = 0.2)',
      'y (t = 0.4)',
      'y (t = 0.5)',
      'y (t = 0.6)',
      'y (t = 0.9)'
    ),
    longtable=FALSE
)
```

```{r include=FALSE}
true_positive_rate <- function(y, y1){
  tp <- sum(y == y1 & y == 1)
  fn <- sum(y != y1 & y == 0)
  return(tp / (tp + fn))
}

false_positive_rate <- function(y, y1){
  fp <- sum(y != y1 & y == 1)
  tn <- sum(y == y1 & y == 0)
  return(fp / (fp + tn))
}

tpr <- c(
  0,
  true_positive_rate(df$y, df$y1),
  true_positive_rate(df$y, df$y2),
  true_positive_rate(df$y, df$y3),
  true_positive_rate(df$y, df$y4),
  true_positive_rate(df$y, df$y5),
  1
)
fpr <- c(
  0,
  false_positive_rate(df$y, df$y1),
  false_positive_rate(df$y, df$y2),
  false_positive_rate(df$y, df$y3),
  false_positive_rate(df$y, df$y4),
  false_positive_rate(df$y, df$y5),
  1
)

df <- data.frame(tpr = tpr, fpr = fpr)
```

(ref:roc-curve-plot) Receiver operating characteristic curve (ROC curve) using data from Table\ \@ref(tab:roc-curve).

```{r roc-curve-plot, echo = FALSE, fig.cap = "(ref:roc-curve-plot)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
ggplot(
  df,
  aes(
    fpr,
    tpr
  )
) +
  geom_point() +
  geom_step() +
  xlim(0, 1) +
  ylim(0, 1) +
  xlab('False positive rate') +
  ylab('True positive rate')
```

The area under the receiver operating characteristic curve (also called AUC or AUROC)
varies between 0 and 1
and
is related with the degree of separation between the positive and negative cases.
AUC is often used for model comparison in machine learning academic papers as
AUC closer to 1 indicates a clear separation between the positive and negative cases
that translates into a better model.

The dataset used
to train artificial intelligence
for classification
is the same used for regression
and
has
images
and
the labels of each image,
see Figure\ \@ref(fig:analyse-medical-image-tasks-regression-training).
Examples of database available for
non-commercial research and educational purposes
and
used to train artificial intelligence
for classification
are
are
PASCAL Visual Object Classes (VOC)\ [@everinghamPascalVisualObject2010; @everinghamPascalVisualObject2015],
ImageNet\ [@dengImageNetLargescaleHierarchical2009],
COCO\ [@linMicrosoftCOCOCommon2015],
and
MURA\ [@rajpurkarMURALargeDataset2018].

<!-- TODO Describe the similarity matching problem 
The classification problem
can be transformed into
the similarity matching problem\ [@maoClassificationHandwristMaturity2021].
-->

## Detection

The detection task,
also referred to as localisation\ [@calliDeepLearningChest2021],
is
to identify if one or more predefined objects appear in an input image
and
provide the position of the objects in the image\ [@litjensSurveyDeepLearning2017; @liuAdvancesDeepLearning2021],
see Figure\ \@ref(fig:analyse-medical-image-tasks-detection-prediction).
The position of the objects in the image is usually represented by
the coordinates of 1 pixel (point location or center of mass) or 2 pixels (bounding box)
and
the choice is application dependent,
for example
use of 1 pixel for circular cells
and
2 pixels for bones.
In the medical imaging domain,
detection is typically used to identify
anatomical regions,
abnormalities
or
foreign objects\ [@calliDeepLearningChest2021].

(ref:analyse-medical-image-tasks-detection-prediction) Illustration of artificial intelligence prediction for detection of femur head in medical image. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-detection-prediction, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-detection-prediction)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/detection-prediction.png'
)
```

A wide range of methods to convert the output of the detection task
into the four possible outcomes of the binary classification task
are available.
One method is based in the distance of the points
that represent the location of the object,
see Figure\ \@ref(fig:analyse-medical-image-tasks-detection-metrics-point),
and was used in\ [@cina2stepDeepLearning2021].
Another method is based in the area of the bounding box
that represent the location of the objects,
see Figure\ \@ref(fig:analyse-medical-image-tasks-detection-metrics-area),
and was used in\ [@everinghamPascalVisualObject2010; @russakovskyDetectingAvocadosZucchinis2013; @linMicrosoftCOCOCommon2015].
After convert the output of the detection task
into the four possible outcomes of the binary classification task,
the same metrics used for the classification task,
accuracy, precision, sensitivity, and AUC,
can be used for the detection task.
For multi-label detection task,
accuracy,
precision,
and
sensitivity
is not appropriate
and some average is used\ [@everinghamPascalVisualObject2010; @russakovskyDetectingAvocadosZucchinis2013; @linMicrosoftCOCOCommon2015].

(ref:analyse-medical-image-tasks-detection-metrics-point) Illustration of distance between points based method for classification of success of detection task of medical image. Distance smaller or equal than a threshold is classified as positive. X-ray of hip by [Mikael Häggström](https://comm.ons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-detection-metrics-point, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-detection-metrics-point)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/detection-metrics-point.png'
)
```

(ref:analyse-medical-image-tasks-detection-metrics-area) Illustration of area based method for classification of success of detection task of medical image. Overlap over union ratio, also called as intersection over union (IoU), is defined as $A_O / A_U$ and classified as positive when larger or equal than a threshold (usually $0.5$). Dice Coefficient is defined as $2 A_O / (A_T + A_P)$ and classified as positive when larger or equal than a threshold (usually $0.5$). X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-detection-metrics-area, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-detection-metrics-area)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/detection-metrics-area.png'
)
```

The database used to train artificial intelligence
for detection
has images,
the location of each object of interest in the image,
and
the labels of each object in the image,
see Figure\ \@ref(fig:analyse-medical-image-tasks-detection-training).
The location of each object of interest in the image
can be provided by the coordinates of pixels
--
usually 1 or 2 pixels are provided
but some studies use other number of pixels,
for example
3 pixels is used in [@raisuddinDeepLearningWrist2020]
and
4 pixels is used in [@cina2stepDeepLearning2021].
Examples of database available for
non-commercial research and educational purposes
and
used to train artificial intelligence
for detection
are
PASCAL Visual Object Classes (VOC)\ [@everinghamPascalVisualObject2010; @everinghamPascalVisualObject2015],
ImageNet\ [@dengImageNetLargescaleHierarchical2009],
and
COCO\ [@linMicrosoftCOCOCommon2015].

(ref:analyse-medical-image-tasks-detection-training) Illustration of artificial intelligence training for detection of medical image. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-detection-training, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-detection-training)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/detection-training.png'
)
```

## Segmentation

The segmentation task,
also referred as "semantic segmentation"\ [@calliDeepLearningChest2021],
is
to identify if one or more predefined objects appear in an input image
and
provide all the pixels that make up the objects in the image\ [@litjensSurveyDeepLearning2017; @liuAdvancesDeepLearning2021],
see Figure\ \@ref(fig:analyse-medical-image-tasks-segmentation-prediction).
The segmentation task can also be defined as
to assign one or more predefined labels to every pixel in an input image\ [@calliDeepLearningChest2021].
In the medical imaging domain,
segmentation is typically used to provide the contour of
anatomical features,
abnormalities
or
foreign objects\ [@calliDeepLearningChest2021].

(ref:analyse-medical-image-tasks-segmentation-prediction) Illustration of artificial intelligence prediction for segmentation of femur head in medical image. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-segmentation-prediction, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-segmentation-prediction)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/segmentation-prediction.png'
)
```

The area based method for classification of success of detection task
can be adapted for the segmentation task
and
the same metrics used for the detection task
can used for the segmentation task\ [@everinghamPascalVisualObject2010; @russakovskyDetectingAvocadosZucchinis2013; @linMicrosoftCOCOCommon2015].

The database used to train artificial intelligence
for segmentation
has
the images
and
the labels of every pixel in the image,
see Figure\ \@ref(fig:analyse-medical-image-tasks-segmentation-training).
The labels of every pixel in the image
can be provided as contour or mask.
Examples of database available for non-commercial research and educational purposes and used to train artificial intelligence for segmentation are
Berkeley Segmentation Data Set and Benchmarks 500\ [@arbelaezContourDetectionHierarchical2011]
and
COCO\ [@linMicrosoftCOCOCommon2015].

(ref:analyse-medical-image-tasks-segmentation-training) Illustration of artificial intelligence training for segmentation of medical image. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-segmentation-training, echo = FALSE, fig.cap = "(ref:analyse-medical-image-tasks-segmentation-training)", fig.show = 'hold', fig.align = 'center', out.width = global_out_width}
knitr::include_graphics(
  './img/segmentation-training.png'
)
```

<!--
## Registration

The registration or spatial alignment task is to
identify corresponding points in the the input image\ [@litjensSurveyDeepLearning2017; @andreDeepLearningenabledMedical2021; @liuAdvancesDeepLearning2021],
see Figure\ \@ref(fig:analyse-medical-image-tasks-segmentation-prediction).
Registration is used to compare images acquired at different times or from different subjects
and
blend images from different modalities
or different viewpoints
for rich visualisation\ [@liuAdvancesDeepLearning2021].
In the medical imaging domain,
registration is typically used to blend computed tomography and magnetic resonance images.
-->

(ref:analyse-medical-image-tasks-registration-prediction) Illustration of artificial intelligence prediction for registration of medical image. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-registration-prediction, fig.align='center', fig.cap="(ref:analyse-medical-image-tasks-registration-prediction)", fig.show='hold', include=FALSE, out.width=global_out_width}
knitr::include_graphics(
  './img/registration-prediction.png'
)
```

<!--
The database used to train artificial intelligence for registration
has the original images and the points of references that will be used for the spatial aligment,
see Figure\ \@ref(fig:analyse-medical-image-tasks-registration-training).
Examples of database available
for non-commercial research and educational purposes and used
to train artificial intelligence for registration are FIRE\ [@hernandez-matasFIREFundusImage2017].
-->

(ref:analyse-medical-image-tasks-registration-training) Illustration of artificial intelligence training for registration of medical image. X-ray of hip by [Mikael Häggström](https://commons.wikimedia.org/wiki/User:Mikael_H%C3%A4ggstr%C3%B6m) used under Creative Commons CC0 1.0 Universal Public Domain Dedication.

```{r analyse-medical-image-tasks-registration-training, fig.align='center', fig.cap="(ref:analyse-medical-image-tasks-registration-training)", fig.show='hold', include=FALSE, out.width=global_out_width}
knitr::include_graphics(
  './img/registration-training.png'
)
```